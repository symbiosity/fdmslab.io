## Symbiosity.co

Serve the site:

```
bin/serve
```

Or you can run the latest Docker image:

```
docker run -p 4000:4000 registry.gitlab.com/symbiosity/fdms.gitlab.io
```
